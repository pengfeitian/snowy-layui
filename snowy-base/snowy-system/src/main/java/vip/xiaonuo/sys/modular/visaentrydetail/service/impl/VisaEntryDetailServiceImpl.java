/*
Copyright [2020] [https://www.xiaonuo.vip]

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

Snowy采用APACHE LICENSE 2.0开源协议，您在使用过程中，需要注意以下几点：

1.请不要删除和修改根目录下的LICENSE文件。
2.请不要删除和修改Snowy源码头部的版权声明。
3.请保留源码和相关描述文件的项目出处，作者声明等。
4.分发源码时候，请注明软件出处 https://gitee.com/xiaonuobase/snowy-layui
5.在修改包名，模块名称，项目代码等时，请注明软件出处 https://gitee.com/xiaonuobase/snowy-layui
6.若您的项目无法满足以上几点，可申请商业授权，获取Snowy商业授权许可，请在官网购买授权，地址为 https://www.xiaonuo.vip
 */
package vip.xiaonuo.sys.modular.visaentrydetail.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import vip.xiaonuo.core.consts.CommonConstant;
import vip.xiaonuo.core.enums.CommonStatusEnum;
import vip.xiaonuo.core.exception.ServiceException;
import vip.xiaonuo.core.factory.PageFactory;
import vip.xiaonuo.core.pojo.page.PageResult;
import vip.xiaonuo.sys.modular.visaentrydetail.entity.VisaEntryDetail;
import vip.xiaonuo.sys.modular.visaentrydetail.enums.VisaEntryDetailExceptionEnum;
import vip.xiaonuo.sys.modular.visaentrydetail.mapper.VisaEntryDetailMapper;
import vip.xiaonuo.sys.modular.visaentrydetail.param.VisaEntryDetailParam;
import vip.xiaonuo.sys.modular.visaentrydetail.service.VisaEntryDetailService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import javax.annotation.Resource;
import java.util.List;

/**
 * 护照入境明细信息service接口实现类
 *
 * @author tpf
 * @date 2024-04-24 11:57:26
 */
@Service
public class VisaEntryDetailServiceImpl extends ServiceImpl<VisaEntryDetailMapper, VisaEntryDetail> implements VisaEntryDetailService {

    @Override
    public PageResult<VisaEntryDetail> page(VisaEntryDetailParam visaEntryDetailParam) {
        QueryWrapper<VisaEntryDetail> queryWrapper = new QueryWrapper<>();
        if (ObjectUtil.isNotNull(visaEntryDetailParam)) {

            // 根据护照信息ID 查询
            if (ObjectUtil.isNotEmpty(visaEntryDetailParam.getVisaId())) {
                queryWrapper.lambda().eq(VisaEntryDetail::getVisaId, visaEntryDetailParam.getVisaId());
            }
            // 根据入境三件套check：0否 1是 查询
            if (ObjectUtil.isNotEmpty(visaEntryDetailParam.getCheck1Status())) {
                queryWrapper.lambda().eq(VisaEntryDetail::getCheck1Status, visaEntryDetailParam.getCheck1Status());
            }
            // 根据入境三件套备注 查询
            if (ObjectUtil.isNotEmpty(visaEntryDetailParam.getCheck1Remark())) {
                queryWrapper.lambda().eq(VisaEntryDetail::getCheck1Remark, visaEntryDetailParam.getCheck1Remark());
            }
            // 根据加入厚生年金保险check：0否 1是 查询
            if (ObjectUtil.isNotEmpty(visaEntryDetailParam.getCheck2Status())) {
                queryWrapper.lambda().eq(VisaEntryDetail::getCheck2Status, visaEntryDetailParam.getCheck2Status());
            }
            // 根据加入厚生年金保险备注 查询
            if (ObjectUtil.isNotEmpty(visaEntryDetailParam.getCheck2Remark())) {
                queryWrapper.lambda().eq(VisaEntryDetail::getCheck2Remark, visaEntryDetailParam.getCheck2Remark());
            }
            // 根据状态（字典 0正常 1停用 2删除） 查询
            if (ObjectUtil.isNotEmpty(visaEntryDetailParam.getStatus())) {
                queryWrapper.lambda().eq(VisaEntryDetail::getStatus, visaEntryDetailParam.getStatus());
            }
        }
        return new PageResult<>(this.page(PageFactory.defaultPage(), queryWrapper));
    }

    @Override
    public List<VisaEntryDetail> list(VisaEntryDetailParam visaEntryDetailParam) {
        return this.list();
    }

    @Override
    public void add(VisaEntryDetailParam visaEntryDetailParam) {
        VisaEntryDetail visaEntryDetail = new VisaEntryDetail();
        BeanUtil.copyProperties(visaEntryDetailParam, visaEntryDetail);
        this.save(visaEntryDetail);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void delete(List<VisaEntryDetailParam> visaEntryDetailParamList) {
        visaEntryDetailParamList.forEach(visaEntryDetailParam -> {
        VisaEntryDetail visaEntryDetail = this.queryVisaEntryDetail(visaEntryDetailParam);
            this.removeById(visaEntryDetail.getId());
        });
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void edit(VisaEntryDetailParam visaEntryDetailParam) {
        VisaEntryDetail visaEntryDetail = this.queryVisaEntryDetail(visaEntryDetailParam);
        BeanUtil.copyProperties(visaEntryDetailParam, visaEntryDetail);
        this.updateById(visaEntryDetail);
    }

    @Override
    public VisaEntryDetail detail(VisaEntryDetailParam visaEntryDetailParam) {
        return this.getOne(new LambdaQueryWrapper<VisaEntryDetail>().eq(VisaEntryDetail::getVisaId,visaEntryDetailParam.getId()));
    }

    /**
     * 获取护照入境明细信息
     *
     * @author tpf
     * @date 2024-04-24 11:57:26
     */
    private VisaEntryDetail queryVisaEntryDetail(VisaEntryDetailParam visaEntryDetailParam) {
        VisaEntryDetail visaEntryDetail = this.getById(visaEntryDetailParam.getId());
        if (ObjectUtil.isNull(visaEntryDetail)) {
            throw new ServiceException(VisaEntryDetailExceptionEnum.NOT_EXIST);
        }
        return visaEntryDetail;
    }
}

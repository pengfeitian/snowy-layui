/*
Copyright [2020] [https://www.xiaonuo.vip]

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

Snowy采用APACHE LICENSE 2.0开源协议，您在使用过程中，需要注意以下几点：

1.请不要删除和修改根目录下的LICENSE文件。
2.请不要删除和修改Snowy源码头部的版权声明。
3.请保留源码和相关描述文件的项目出处，作者声明等。
4.分发源码时候，请注明软件出处 https://gitee.com/xiaonuobase/snowy-layui
5.在修改包名，模块名称，项目代码等时，请注明软件出处 https://gitee.com/xiaonuobase/snowy-layui
6.若您的项目无法满足以上几点，可申请商业授权，获取Snowy商业授权许可，请在官网购买授权，地址为 https://www.xiaonuo.vip
 */
package vip.xiaonuo.sys.modular.visaentrydetail.param;

import vip.xiaonuo.core.pojo.base.param.BaseParam;
import lombok.Data;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.NotBlank;
import java.util.*;

/**
* 护照入境明细信息参数类
 *
 * @author tpf
 * @date 2024-04-24 11:57:26
*/
@Data
public class VisaEntryDetailParam extends BaseParam {

    /**
     * 主键
     */
    private String id;

    /**
     * 护照信息ID
     */
    private String visaId;

    /**
     * 入境三件套check：0否 1是
     */
    private Integer check1Status;

    /**
     * 入境三件套备注
     */
    private String check1Remark;

    /**
     * 加入厚生年金保险check：0否 1是
     */
    private Integer check2Status;

    /**
     * 加入厚生年金保险备注
     */
    private String check2Remark;

    /**
     * 状态（字典 0正常 1停用 2删除）
     */
    private Integer status;

}

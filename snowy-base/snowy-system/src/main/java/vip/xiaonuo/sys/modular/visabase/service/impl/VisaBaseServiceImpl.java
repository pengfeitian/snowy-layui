/*
Copyright [2020] [https://www.xiaonuo.vip]

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

Snowy采用APACHE LICENSE 2.0开源协议，您在使用过程中，需要注意以下几点：

1.请不要删除和修改根目录下的LICENSE文件。
2.请不要删除和修改Snowy源码头部的版权声明。
3.请保留源码和相关描述文件的项目出处，作者声明等。
4.分发源码时候，请注明软件出处 https://gitee.com/xiaonuobase/snowy-layui
5.在修改包名，模块名称，项目代码等时，请注明软件出处 https://gitee.com/xiaonuobase/snowy-layui
6.若您的项目无法满足以上几点，可申请商业授权，获取Snowy商业授权许可，请在官网购买授权，地址为 https://www.xiaonuo.vip
 */
package vip.xiaonuo.sys.modular.visabase.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.json.JSONUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import vip.xiaonuo.core.consts.CommonConstant;
import vip.xiaonuo.core.context.login.LoginContextHolder;
import vip.xiaonuo.core.enums.CommonStatusEnum;
import vip.xiaonuo.core.exception.ServiceException;
import vip.xiaonuo.core.factory.PageFactory;
import vip.xiaonuo.core.pojo.page.PageResult;
import vip.xiaonuo.sys.modular.msg.enums.SysMessageTypeEnum;
import vip.xiaonuo.sys.modular.msg.service.SysMessageService;
import vip.xiaonuo.sys.modular.visabase.entity.VisaBase;
import vip.xiaonuo.sys.modular.visabase.enums.VisaBaseExceptionEnum;
import vip.xiaonuo.sys.modular.visabase.mapper.VisaBaseMapper;
import vip.xiaonuo.sys.modular.visabase.param.VisaBaseParam;
import vip.xiaonuo.sys.modular.visabase.service.VisaBaseService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import vip.xiaonuo.sys.modular.visabase.vo.VisaBaseVO;
import vip.xiaonuo.sys.modular.visaentrydetail.entity.VisaEntryDetail;
import vip.xiaonuo.sys.modular.visaentrydetail.service.VisaEntryDetailService;

import javax.annotation.Resource;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.*;
import java.util.stream.Collectors;

/**
 * 护照基本信息service接口实现类
 *
 * @author tpf
 * @date 2024-04-22 13:13:18
 */
@Service
public class VisaBaseServiceImpl extends ServiceImpl<VisaBaseMapper, VisaBase> implements VisaBaseService {

    @Resource
    private SysMessageService sysMessageService;

    @Resource
    private VisaEntryDetailService visaEntryDetailService;

    @Override
    public PageResult<VisaBase> page(VisaBaseParam visaBaseParam) {
        QueryWrapper<VisaBase> queryWrapper = new QueryWrapper<>();
        if (ObjectUtil.isNotNull(visaBaseParam)) {

            // 根据英文名称 查询
            if (ObjectUtil.isNotEmpty(visaBaseParam.getEnName())) {
                queryWrapper.lambda().like(VisaBase::getEnName, visaBaseParam.getEnName());
            }
            // 根据中文名称 查询
            if (ObjectUtil.isNotEmpty(visaBaseParam.getCnName())) {
                queryWrapper.lambda().like(VisaBase::getCnName, visaBaseParam.getCnName());
            }
            // 根据护照编号 查询
            if (ObjectUtil.isNotEmpty(visaBaseParam.getVisaNo())) {
                queryWrapper.lambda().like(VisaBase::getVisaNo, visaBaseParam.getVisaNo());
            }
            // 根据手机号 查询
            if (ObjectUtil.isNotEmpty(visaBaseParam.getUserMobile())) {
                queryWrapper.lambda().eq(VisaBase::getUserMobile, visaBaseParam.getUserMobile());
            }
            // 根据是否入境
            if (ObjectUtil.isNotEmpty(visaBaseParam.getEntrustStatus())) {
                queryWrapper.lambda().eq(VisaBase::getEntrustStatus, visaBaseParam.getEntrustStatus());
            }
            // 根据签证类型1.经管签（4个月）2.经管签（1年）3.工作签 查询
            if (ObjectUtil.isNotEmpty(visaBaseParam.getVisaType())) {
                queryWrapper.lambda().eq(VisaBase::getVisaType, visaBaseParam.getVisaType());
            }
        }
        PageResult<VisaBase> visaBasePageResult = new PageResult<>(this.page(PageFactory.defaultPage(), queryWrapper));
        List<VisaBase> data = visaBasePageResult.getData();
        //已入境集合 入境时间 不为空
        List<VisaBase> entryList = data.stream().filter(x -> x.getEntryStatus().equals(1) && ObjectUtil.isNotEmpty(x.getEntryTime())).collect(Collectors.toList());



        return new PageResult<>(this.page(PageFactory.defaultPage(), queryWrapper));
    }

    @Override
    public List<VisaBase> list(VisaBaseParam visaBaseParam) {
        return this.list();
    }

    /**
     *  入境材料收集提醒
     * @return
     */
    @Override
    public List<String> getEntryTip() {
        List<String> msgList = new ArrayList<>();
        List<VisaBase> entryList = this.list(new LambdaQueryWrapper<VisaBase>().eq(VisaBase::getEntryStatus, 1).isNotNull(VisaBase::getEntryTime));
        if(CollectionUtil.isNotEmpty(entryList)){
            List<String> visaIdList = entryList.stream().map(VisaBase::getId).collect(Collectors.toList());
            //资料未收集完成集合
            List<VisaEntryDetail> detailList = visaEntryDetailService.list(new LambdaQueryWrapper<VisaEntryDetail>().eq(VisaEntryDetail::getCheck1Status, 0).in(VisaEntryDetail::getVisaId, visaIdList));
            if(CollectionUtil.isNotEmpty(detailList)){
                List<String> noFillVisaIdList = detailList.stream().map(VisaEntryDetail::getVisaId).distinct().collect(Collectors.toList());
                List<VisaBase> noFillVisaList = entryList.stream().filter(x -> noFillVisaIdList.contains(x.getId())).collect(Collectors.toList());
                for (VisaBase visaBase : noFillVisaList) {
                    LocalDate date1 = LocalDate.parse(visaBase.getEntryTime());
                    LocalDate date2 = LocalDate.parse(new SimpleDateFormat("yyyy-MM-dd").format(new Date()));
                    long daysBetween = ChronoUnit.DAYS.between(date1, date2);
                    if(daysBetween > 7){
                        String msgModel = "【中文名称:%s,护照编号为:%s,入境三件套7日内未完成,入境日期为:%s】";
                        msgList.add(String.format(msgModel,visaBase.getCnName(),visaBase.getVisaNo(),visaBase.getEntryTime()));
                    }
                }

            }
        }
        return msgList;
    }


    /**
     * 在留卡期限判断
     * @return
     */
    @Override
    public List<String> getEntryEndTimeTip() {
        List<String> msgList = new ArrayList<>();
        //已入境 在留卡期限不为空
        List<VisaBase> entryList = this.list(new LambdaQueryWrapper<VisaBase>().eq(VisaBase::getEntryStatus, 1).isNotNull(VisaBase::getEntryEndTime));
        if(CollectionUtil.isNotEmpty(entryList)){
            for (VisaBase visaBase : entryList) {
                LocalDate date1 = LocalDate.parse(new SimpleDateFormat("yyyy-MM-dd").format(new Date()));
                LocalDate date2 = LocalDate.parse(visaBase.getEntryEndTime());
                long daysBetween = ChronoUnit.DAYS.between(date1, date2);
                if(daysBetween < 90){
                        String msgModel = "【中文名称:,护照编号为:%s,在留卡时间为:%s,剩余:%s天,已不满90天】";
                        msgList.add(String.format(msgModel,visaBase.getCnName(),visaBase.getVisaNo(),visaBase.getEntryEndTime(),daysBetween >0 ? daysBetween:0));
               }
            }
        }
        return msgList;
    }


    /**
     * 厚生年金缴费提醒
     * @return
     */
    @Override
    public List<String> payTips() {
        List<String> msgList = new ArrayList<>();
        LocalDate today = LocalDate.now();
        int dayOfMonth = today.getDayOfMonth();
        if (dayOfMonth == 25) {
            System.out.println("今天是25号");
            //获取已入境数据
            List<VisaBase> entryList = this.list(new LambdaQueryWrapper<VisaBase>().eq(VisaBase::getEntryStatus, 1).isNotNull(VisaBase::getEntryTime));
            if(CollectionUtil.isNotEmpty(entryList)){
                List<String> visaIdList = entryList.stream().map(VisaBase::getId).collect(Collectors.toList());
                //入境资料收集完成  厚生年金缴费未完成
                List<VisaEntryDetail> detailList = visaEntryDetailService.list(new LambdaQueryWrapper<VisaEntryDetail>().eq(VisaEntryDetail::getCheck1Status, 2).eq(VisaEntryDetail::getCheck2Status, 0)
                        .in(VisaEntryDetail::getVisaId, visaIdList));
                if(CollectionUtil.isNotEmpty(detailList)){
                    List<String> noFillVisaIdList = detailList.stream().map(VisaEntryDetail::getVisaId).distinct().collect(Collectors.toList());
                    List<VisaBase> noFillVisaList = entryList.stream().filter(x -> noFillVisaIdList.contains(x.getId())).collect(Collectors.toList());
                    for (VisaBase visaBase : noFillVisaList) {
                        String msgModel = "【中文名称:%s,护照编号为:%s,厚生年金缴费未完成】";
                        msgList.add(String.format(msgModel,visaBase.getCnName(),visaBase.getVisaNo()));
                    }
                }
            }
        }
        return msgList;
    }

    @Override
    public List<String> getFamilyEntryEndTimeTip() {
        List<String> msgList = new ArrayList<>();
        //已入境 在留卡期限不为空 有家属 家属已入境 家属留卡期限不为空
        List<VisaBase> entryList = this.list(new LambdaQueryWrapper<VisaBase>()
                .eq(VisaBase::getEntryStatus, 1)
                .eq(VisaBase::getFamilyStatus, 1)
                .eq(VisaBase::getFamilyEntryStatus, 1)
                .isNotNull(VisaBase::getEntryEndTime)
                .isNotNull(VisaBase::getFamilyEndTime));
        if(CollectionUtil.isNotEmpty(entryList)){
            for (VisaBase visaBase : entryList) {
                String familyEndTime = visaBase.getFamilyEndTime();
                String[] splitFamilyEndTime = familyEndTime.split(",");
                for (String singleFamilyEndTime : splitFamilyEndTime) {
                    LocalDate date1 = LocalDate.parse(new SimpleDateFormat("yyyy-MM-dd").format(new Date()));
                    LocalDate date2 = LocalDate.parse(singleFamilyEndTime);
                    long daysBetween = ChronoUnit.DAYS.between(date1, date2);
                    if(daysBetween < 90){
                        String msgModel = "【中文名称:%s,护照编号为:%s,家属在留卡时间为:%s,剩余:%s天,已不满90天】";
                        msgList.add(String.format(msgModel,visaBase.getCnName(),visaBase.getVisaNo(),singleFamilyEndTime,daysBetween >0 ? daysBetween:0));
                        break;
                    }
                }


            }
        }
        return msgList;
    }

    @Override
    public void add(VisaBaseParam visaBaseParam) {
        VisaBase visaBase = new VisaBase();
        BeanUtil.copyProperties(visaBaseParam, visaBase);
        this.save(visaBase);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void delete(List<VisaBaseParam> visaBaseParamList) {
        visaBaseParamList.forEach(visaBaseParam -> {
        VisaBase visaBase = this.queryVisaBase(visaBaseParam);
            this.removeById(visaBase.getId());
        });
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void edit(VisaBaseParam visaBaseParam) {
        VisaBase visaBase = this.queryVisaBase(visaBaseParam);
        BeanUtil.copyProperties(visaBaseParam, visaBase);
        this.updateById(visaBase);
    }

    @Override
    public VisaBase detail(VisaBaseParam visaBaseParam) {
        return this.queryVisaBase(visaBaseParam);
    }

    /**
     * 获取护照基本信息
     *
     * @author tpf
     * @date 2024-04-22 13:13:18
     */
    private VisaBase queryVisaBase(VisaBaseParam visaBaseParam) {
        VisaBase visaBase = this.getById(visaBaseParam.getId());
        if (ObjectUtil.isNull(visaBase)) {
            throw new ServiceException(VisaBaseExceptionEnum.NOT_EXIST);
        }
        return visaBase;
    }
}

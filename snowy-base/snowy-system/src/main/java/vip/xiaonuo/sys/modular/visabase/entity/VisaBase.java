/*
Copyright [2020] [https://www.xiaonuo.vip]

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

Snowy采用APACHE LICENSE 2.0开源协议，您在使用过程中，需要注意以下几点：

1.请不要删除和修改根目录下的LICENSE文件。
2.请不要删除和修改Snowy源码头部的版权声明。
3.请保留源码和相关描述文件的项目出处，作者声明等。
4.分发源码时候，请注明软件出处 https://gitee.com/xiaonuobase/snowy-layui
5.在修改包名，模块名称，项目代码等时，请注明软件出处 https://gitee.com/xiaonuobase/snowy-layui
6.若您的项目无法满足以上几点，可申请商业授权，获取Snowy商业授权许可，请在官网购买授权，地址为 https://www.xiaonuo.vip
 */
package vip.xiaonuo.sys.modular.visabase.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import vip.xiaonuo.core.pojo.base.entity.BaseEntity;

/**
 * 护照基本信息
 *
 * @author tpf
 * @date 2024-04-22 13:13:18
 */
@EqualsAndHashCode(callSuper = true)
@Data
@TableName("visa_base")
public class VisaBase extends BaseEntity {

    /**
     * 主键
     */
    @TableId(type = IdType.ASSIGN_ID)
    private String id;

    /**
     * 英文名称
     */
    private String enName;

    /**
     * 中文名称
     */
    private String cnName;

    /**
     * 护照编号
     */
    private String visaNo;

    /**
     * 出生日期
     */
    private String userBirthday;

    /**
     * 手机号
     */
    private String userMobile;

    /**
     * 用户邮箱
     */
    private String userEmail;

    /**
     * 签证类型1.经管签（4个月）2.经管签（1年）3.工作签
     */
    private Integer visaType;


    /**
     * 用户照片
     */
    private String userPicture;


    /**
     * 是否入境：0否 1是
     */
    private Integer entryStatus;

    /**
     * 入境日期
     */
    private String entryTime;

    /**
     * 在留卡期限
     */
    private String entryEndTime;

    /**
     * 是否有家属：0否 1是
     */
    private Integer familyStatus;

    /**
     * 家属是否入境：0否 1是
     */
    private Integer familyEntryStatus;

    /**
     * 家属在留卡期限(多个逗号隔开)
     */
    private String familyEndTime;

    /**
     * 是否委托公司：0否 1是
     */
    private Integer entrustStatus;

    /**
     * 是否做决算：0否 1是
     */
    private Integer finalAccountsStatus;

    /**
     * 公司决算日期
     */
    private String finalAccountsTime;

    /**
     * 公司成立日期
     */
    private String incorporationTime;


    /**
     * 状态（字典 0正常 1停用 2删除）
     */
    private Integer status;

}

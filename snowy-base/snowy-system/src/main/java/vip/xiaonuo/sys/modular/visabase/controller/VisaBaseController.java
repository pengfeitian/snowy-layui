/*
Copyright [2020] [https://www.xiaonuo.vip]

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

Snowy采用APACHE LICENSE 2.0开源协议，您在使用过程中，需要注意以下几点：

1.请不要删除和修改根目录下的LICENSE文件。
2.请不要删除和修改Snowy源码头部的版权声明。
3.请保留源码和相关描述文件的项目出处，作者声明等。
4.分发源码时候，请注明软件出处 https://gitee.com/xiaonuobase/snowy-layui
5.在修改包名，模块名称，项目代码等时，请注明软件出处 https://gitee.com/xiaonuobase/snowy-layui
6.若您的项目无法满足以上几点，可申请商业授权，获取Snowy商业授权许可，请在官网购买授权，地址为 https://www.xiaonuo.vip
 */
package vip.xiaonuo.sys.modular.visabase.controller;

import vip.xiaonuo.core.annotion.BusinessLog;
import vip.xiaonuo.core.annotion.Permission;
import vip.xiaonuo.core.enums.LogAnnotionOpTypeEnum;
import vip.xiaonuo.core.pojo.page.PageResult;
import vip.xiaonuo.core.pojo.response.ResponseData;
import vip.xiaonuo.core.pojo.response.SuccessResponseData;
import vip.xiaonuo.sys.modular.visabase.entity.VisaBase;
import vip.xiaonuo.sys.modular.visabase.param.VisaBaseParam;
import vip.xiaonuo.sys.modular.visabase.service.VisaBaseService;
import org.springframework.stereotype.Controller;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import vip.xiaonuo.sys.modular.visabase.vo.VisaBaseVO;

import javax.annotation.Resource;
import java.util.List;

/**
 * 护照基本信息控制器
 *
 * @author tpf
 * @date 2024-04-22 13:13:18
 */
@Controller
public class VisaBaseController {

    private String PATH_PREFIX = "visaBase/";

    @Resource
    private VisaBaseService visaBaseService;

    /**
     * 护照基本信息页面
     *
     * @author tpf
     * @date 2024-04-22 13:13:18
     */
    @GetMapping("/visaBase/index")
    public String index() {
        return PATH_PREFIX + "index.html";
    }

    /**
     * 护照基本信息表单页面
     *
     * @author tpf
     * @date 2024-04-22 13:13:18
     */
    @GetMapping("/visaBase/form")
    public String form() {
        return PATH_PREFIX + "form.html";
    }

    /**
     * 查询护照基本信息
     *
     * @author tpf
     * @date 2024-04-22 13:13:18
     */
    @ResponseBody
    @GetMapping("/visaBase/page")
    @BusinessLog(title = "护照基本信息_查询", opType = LogAnnotionOpTypeEnum.QUERY)
    public PageResult<VisaBase> page(VisaBaseParam visaBaseParam) {
        return visaBaseService.page(visaBaseParam);
    }

    /**
     * 添加护照基本信息
     *
     * @author tpf
     * @date 2024-04-22 13:13:18
     */
    @ResponseBody
    @PostMapping("/visaBase/add")
    @BusinessLog(title = "护照基本信息_增加", opType = LogAnnotionOpTypeEnum.ADD)
    public ResponseData add(@RequestBody @Validated(VisaBaseParam.add.class) VisaBaseParam visaBaseParam) {
        visaBaseService.add(visaBaseParam);
        return new SuccessResponseData();
    }

    /**
     * 删除护照基本信息
     *
     * @author tpf
     * @date 2024-04-22 13:13:18
     */
    @ResponseBody
    @PostMapping("/visaBase/delete")
    @BusinessLog(title = "护照基本信息_删除", opType = LogAnnotionOpTypeEnum.DELETE)
    public ResponseData delete(@RequestBody @Validated(VisaBaseParam.delete.class) List<VisaBaseParam> visaBaseParamList) {
        visaBaseService.delete(visaBaseParamList);
        return new SuccessResponseData();
    }

    /**
     * 编辑护照基本信息
     *
     * @author tpf
     * @date 2024-04-22 13:13:18
     */
    @ResponseBody
    @PostMapping("/visaBase/edit")
    @BusinessLog(title = "护照基本信息_编辑", opType = LogAnnotionOpTypeEnum.EDIT)
    public ResponseData edit(@RequestBody @Validated(VisaBaseParam.edit.class) VisaBaseParam visaBaseParam) {
        visaBaseService.edit(visaBaseParam);
        return new SuccessResponseData();
    }

    /**
     * 查看护照基本信息
     *
     * @author tpf
     * @date 2024-04-22 13:13:18
     */
    @ResponseBody
    @GetMapping("/visaBase/detail")
    @BusinessLog(title = "护照基本信息_查看", opType = LogAnnotionOpTypeEnum.DETAIL)
    public ResponseData detail(@Validated(VisaBaseParam.detail.class) VisaBaseParam visaBaseParam) {
        return new SuccessResponseData(visaBaseService.detail(visaBaseParam));
    }

    /**
     * 护照基本信息列表
     *
     * @author tpf
     * @date 2024-04-22 13:13:18
     */
    @ResponseBody
    @GetMapping("/visaBase/list")
    @BusinessLog(title = "护照基本信息_列表", opType = LogAnnotionOpTypeEnum.QUERY)
    public ResponseData list(VisaBaseParam visaBaseParam) {
        return new SuccessResponseData(visaBaseService.list(visaBaseParam));
    }


    /**
     * 入境三件套提醒
     * @return
     */
    @ResponseBody
    @GetMapping("/visaBase/entryTip")
    public ResponseData entryTip() {
        return new SuccessResponseData(visaBaseService.getEntryTip());
    }


    /**
     * 厚生年金缴费提醒
     * @return
     */
    @ResponseBody
    @GetMapping("/visaBase/payTip")
    public ResponseData payTip() {
        return new SuccessResponseData(visaBaseService.payTips());
    }

    /**
     * 在留卡期限到期提醒
     * @return
     */
    @ResponseBody
    @GetMapping("/visaBase/entryEndTimeTip")
    public ResponseData entryEndTimeTip() {
        return new SuccessResponseData(visaBaseService.getEntryEndTimeTip());
    }


    /**
     * 家属在留卡期限到期提醒
     * @return
     */
    @ResponseBody
    @GetMapping("/visaBase/familyEntryEndTimeTip")
    public ResponseData familyEntryEndTimeTip() {
        return new SuccessResponseData(visaBaseService.getFamilyEntryEndTimeTip());
    }
}

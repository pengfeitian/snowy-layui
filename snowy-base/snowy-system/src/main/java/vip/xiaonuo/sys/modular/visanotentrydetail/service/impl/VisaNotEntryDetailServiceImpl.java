/*
Copyright [2020] [https://www.xiaonuo.vip]

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

Snowy采用APACHE LICENSE 2.0开源协议，您在使用过程中，需要注意以下几点：

1.请不要删除和修改根目录下的LICENSE文件。
2.请不要删除和修改Snowy源码头部的版权声明。
3.请保留源码和相关描述文件的项目出处，作者声明等。
4.分发源码时候，请注明软件出处 https://gitee.com/xiaonuobase/snowy-layui
5.在修改包名，模块名称，项目代码等时，请注明软件出处 https://gitee.com/xiaonuobase/snowy-layui
6.若您的项目无法满足以上几点，可申请商业授权，获取Snowy商业授权许可，请在官网购买授权，地址为 https://www.xiaonuo.vip
 */
package vip.xiaonuo.sys.modular.visanotentrydetail.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import vip.xiaonuo.core.consts.CommonConstant;
import vip.xiaonuo.core.enums.CommonStatusEnum;
import vip.xiaonuo.core.exception.ServiceException;
import vip.xiaonuo.core.factory.PageFactory;
import vip.xiaonuo.core.pojo.page.PageResult;
import vip.xiaonuo.sys.modular.visanotentrydetail.entity.VisaNotEntryDetail;
import vip.xiaonuo.sys.modular.visanotentrydetail.enums.VisaNotEntryDetailExceptionEnum;
import vip.xiaonuo.sys.modular.visanotentrydetail.mapper.VisaNotEntryDetailMapper;
import vip.xiaonuo.sys.modular.visanotentrydetail.param.VisaNotEntryDetailParam;
import vip.xiaonuo.sys.modular.visanotentrydetail.service.VisaNotEntryDetailService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import javax.annotation.Resource;
import java.util.List;

/**
 * 护照未入境明细信息service接口实现类
 *
 * @author tpf
 * @date 2024-04-24 15:51:56
 */
@Service
public class VisaNotEntryDetailServiceImpl extends ServiceImpl<VisaNotEntryDetailMapper, VisaNotEntryDetail> implements VisaNotEntryDetailService {

    @Override
    public PageResult<VisaNotEntryDetail> page(VisaNotEntryDetailParam visaNotEntryDetailParam) {
        QueryWrapper<VisaNotEntryDetail> queryWrapper = new QueryWrapper<>();
        if (ObjectUtil.isNotNull(visaNotEntryDetailParam)) {

            // 根据护照信息ID 查询
            if (ObjectUtil.isNotEmpty(visaNotEntryDetailParam.getVisaId())) {
                queryWrapper.lambda().eq(VisaNotEntryDetail::getVisaId, visaNotEntryDetailParam.getVisaId());
            }
            // 根据材料收集check：0否 1是 查询
            if (ObjectUtil.isNotEmpty(visaNotEntryDetailParam.getCheck1Status())) {
                queryWrapper.lambda().eq(VisaNotEntryDetail::getCheck1Status, visaNotEntryDetailParam.getCheck1Status());
            }
            // 根据材料收集备注 查询
            if (ObjectUtil.isNotEmpty(visaNotEntryDetailParam.getCheck1Remark())) {
                queryWrapper.lambda().eq(VisaNotEntryDetail::getCheck1Remark, visaNotEntryDetailParam.getCheck1Remark());
            }
            // 根据公司进展check：0否 1是 查询
            if (ObjectUtil.isNotEmpty(visaNotEntryDetailParam.getCheck2Status())) {
                queryWrapper.lambda().eq(VisaNotEntryDetail::getCheck2Status, visaNotEntryDetailParam.getCheck2Status());
            }
            // 根据公司进展备注 查询
            if (ObjectUtil.isNotEmpty(visaNotEntryDetailParam.getCheck2Remark())) {
                queryWrapper.lambda().eq(VisaNotEntryDetail::getCheck2Remark, visaNotEntryDetailParam.getCheck2Remark());
            }
            // 根据提交coe申请check：0否 1是 查询
            if (ObjectUtil.isNotEmpty(visaNotEntryDetailParam.getCheck3Status())) {
                queryWrapper.lambda().eq(VisaNotEntryDetail::getCheck3Status, visaNotEntryDetailParam.getCheck3Status());
            }
            // 根据提交coe申请备注 查询
            if (ObjectUtil.isNotEmpty(visaNotEntryDetailParam.getCheck3Remark())) {
                queryWrapper.lambda().eq(VisaNotEntryDetail::getCheck3Remark, visaNotEntryDetailParam.getCheck3Remark());
            }
            // 根据下签check：0否 1是 查询
            if (ObjectUtil.isNotEmpty(visaNotEntryDetailParam.getCheck4Status())) {
                queryWrapper.lambda().eq(VisaNotEntryDetail::getCheck4Status, visaNotEntryDetailParam.getCheck4Status());
            }
            // 根据下签时间 查询
            if (ObjectUtil.isNotEmpty(visaNotEntryDetailParam.getCheck4Remark())) {
                queryWrapper.lambda().eq(VisaNotEntryDetail::getCheck4Remark, visaNotEntryDetailParam.getCheck4Remark());
            }
            // 根据返签check：0否 1是 查询
            if (ObjectUtil.isNotEmpty(visaNotEntryDetailParam.getCheck5Status())) {
                queryWrapper.lambda().eq(VisaNotEntryDetail::getCheck5Status, visaNotEntryDetailParam.getCheck5Status());
            }
            // 根据返签时间 查询
            if (ObjectUtil.isNotEmpty(visaNotEntryDetailParam.getCheck5Time())) {
                queryWrapper.lambda().eq(VisaNotEntryDetail::getCheck5Time, visaNotEntryDetailParam.getCheck5Time());
            }
            // 根据状态（字典 0正常 1停用 2删除） 查询
            if (ObjectUtil.isNotEmpty(visaNotEntryDetailParam.getStatus())) {
                queryWrapper.lambda().eq(VisaNotEntryDetail::getStatus, visaNotEntryDetailParam.getStatus());
            }
        }
        return new PageResult<>(this.page(PageFactory.defaultPage(), queryWrapper));
    }

    @Override
    public List<VisaNotEntryDetail> list(VisaNotEntryDetailParam visaNotEntryDetailParam) {
        return this.list();
    }

    @Override
    public void add(VisaNotEntryDetailParam visaNotEntryDetailParam) {
        VisaNotEntryDetail visaNotEntryDetail = new VisaNotEntryDetail();
        BeanUtil.copyProperties(visaNotEntryDetailParam, visaNotEntryDetail);
        this.save(visaNotEntryDetail);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void delete(List<VisaNotEntryDetailParam> visaNotEntryDetailParamList) {
        visaNotEntryDetailParamList.forEach(visaNotEntryDetailParam -> {
        VisaNotEntryDetail visaNotEntryDetail = this.queryVisaNotEntryDetail(visaNotEntryDetailParam);
            this.removeById(visaNotEntryDetail.getId());
        });
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void edit(VisaNotEntryDetailParam visaNotEntryDetailParam) {
        VisaNotEntryDetail visaNotEntryDetail = this.queryVisaNotEntryDetail(visaNotEntryDetailParam);
        BeanUtil.copyProperties(visaNotEntryDetailParam, visaNotEntryDetail);
        this.updateById(visaNotEntryDetail);
    }

    @Override
    public VisaNotEntryDetail detail(VisaNotEntryDetailParam visaNotEntryDetailParam) {
        return this.getOne(new LambdaQueryWrapper<VisaNotEntryDetail>().eq(VisaNotEntryDetail::getVisaId,visaNotEntryDetailParam.getId()));
    }

    /**
     * 获取护照未入境明细信息
     *
     * @author tpf
     * @date 2024-04-24 15:51:56
     */
    private VisaNotEntryDetail queryVisaNotEntryDetail(VisaNotEntryDetailParam visaNotEntryDetailParam) {
        VisaNotEntryDetail visaNotEntryDetail = this.getById(visaNotEntryDetailParam.getId());
        if (ObjectUtil.isNull(visaNotEntryDetail)) {
            throw new ServiceException(VisaNotEntryDetailExceptionEnum.NOT_EXIST);
        }
        return visaNotEntryDetail;
    }
}
